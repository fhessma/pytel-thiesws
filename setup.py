from distutils.core import setup, Extension
from distutils.sysconfig import get_python_lib
import os

# get path to numpy headers
numpy_dir = os.path.join(get_python_lib(plat_specific=1), 'numpy/core/include/')

setup(
    name='pytel-thiesws',
    version='0.1',
    packages=['pytel_thiesws'],
    ext_modules=[],
    url='',
    license='',
    author='Frederic V. Hessman',
    author_email='hessman@astro.physik.uni-goettingen.de',
    description='pytel module for Thies weatherstation dataloggers',
    requires=['pyserial']
)
