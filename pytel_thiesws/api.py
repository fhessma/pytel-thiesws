# api.py

"""
Interface definition for Thies weatherstations and Datalogger 9.1740.XX.00X

The byte-based command syntax is

	{STX} {cmd} {arg1} ... {argn} {ETX}

where

	{STX} = Ctrl-B = 0x02
	{cmd} = 2-byte command names
	{arg} = 0..5 individual byte arguments
	{ETX} = Strg-C = 0x03

for a total of 4-9 bytes.

The thies_* functions do not actually talk to the weatherstation - they just
provide the commands that should be sent.

F.Hessman (2019-JAN-9)
"""

import logging

STX = 0x02
ETX = 0x03

thies_commands = {
	'one_day_means' : {
		'cmd': 'ds',
		'format': '{0:b}{1:b}{2:b}{3:b}{4:b}{5:b}',
		'args': '{day+28}{month+28}{year+28, no century}{hour+28}{minute+28}',
		'formats': ['{0}'],
		'help': 'output one day\'s stored data (mean values)',
		},
	'one_day_extremes': {
		'cmd': 'de',
		'format': '{0:b}{1:b}{2:b}',
		'args': '{day+28}{month+28}{year+28, no century}{hour+28}{minute+28}',
		'formats': ['{0}'],
		'help': 'output one day\'s stored data (extreme values)'
		},
	'get_date': {
		'cmd': 'DD',
		'format': None,
		'args': None,
		'formats': ['{0}'],
		'help': 'get logger date'
		},
	'set_year': {
		'cmd': 'DJ',
		'format': '{0:b}',
		'args': '{year, 0..99, without century}',
		'formats': None,
		'help': 'set year, without century'
		},
	'set_month': {
		'cmd': 'DM',
		'format': '{0:b}',
		'args': '{month, 1..12}',
		'formats': None,
		'help': 'set month'
		},
	'set_day': {
		'cmd': 'DT',
		'format': '{0:b}',
		'args': '{day, 1..31}',
		'formats': None,
		'help': 'set day'
		},
	'all_extremes': {
		'cmd': 'EE',
		'format': None,
		'args': None,
		'formats': ['{0}'],
		'help': 'extreme value memory: output of extreme values'
		},
	'all_means': {
		'cmd': 'GS',
		'format': None,
		'args': None,
		'formats': ['{0}'],
		'help': 'entire mean value memory (after re-initialization)'
		},
	'list_commmands': {
		'cmd': 'HH',
		'format': None,
		'args': None,
		'formats': ['{0}'],
		'help': 'shows the list of input commands'
		},
	'sensor_activation': {
		'cmd': 'KK',
		'format': '{0:b}{1:b}',
		'args': '{channel-#, 1..14}{boolean, 1=activate, 0=deactivate}',
		'formats': None,
		'help': '"channel" configuration for (de-)activation of individual sensors'
		},
	'stored_extremes': {
		'cmd': 'le',
		'format': '{0:b}',
		'args': '{storage-#, 1..99}',
		'formats': ['{0}'],
		'help': 'extreme value records stored till now'
		},
	'logger_status': {
		'cmd': 'LL',
		'format': None,
		'args': None,
		'formats': ['{0}'],
		'help': 'get logger status: date,time,A/D status,battery voltage, cycles, timer'
		},
	'last_mean': {
		'cmd': 'LS',
		'format': None,
		'args': None,
		'formats': ['{0}'],
		'help': 'last record from the mean value memory (see MMM)'
		},
	'stored_means': {
		'cmd': 'ls',
		'format': '{0:b}',
		'args': '{storage-#, 1-99}',
		'formats': ['{0}'],
		'help': 'mean value records stored till now'
		},
	'get_weather': {
		'cmd': 'MM',
		'format': None,
		'args': None,
		'formats': ['{0}'],
		'help': 'get all measured values with sensor identification'
		},
	'get_time_weather': {
		'cmd': 'mm',
		'format': None,
		'args': None,
		'formats': ['{0}'],
		'help': 'get all measured values, include time in seconds'
		},
	'set_altitude': {
		'cmd': 'SH',
		'format': '{0:b}',
		'args': '{altitude-in-meters, 0..4000}',
		'formats': None,
		'help': 'set station height (how formatted?)'
		},
	'set_rad_const': {
		'cmd': 'SK',
		'format': '{0:b}',
		'args': '{radiation-constant}',
		'formats': None,
		'help': 'set constant of radiation (how formatted?)'
		},
	'saved_means': {
		'cmd': 'SS',
		'format': None,
		'args': None,
		'formats': ['{0}'],
		'help': 'mean value memory: output of saved mean values'
		},
	'get_extremes': {
		'cmd': 'te',
		'format': '{0:b}{1:b}{2:b}',
		'args': '{day+28}{month+28}{year+28, no century}',
		'formats': ['{0}'],
		'help': 'output one day\'s stored data (extreme values)'
		},
	'get_means': {
		'cmd': 'ts',
		'format': '{0:b}{1:b}{2:b}',
		'args': '{day+28}{month+28}{year+28, no century}',
		'formats': ['{0}'],
		'help': 'output one day\'s stored data (mean values)',
		},
	'get_info': {
		'cmd': 'XX',
		'format': None,
		'args': None,
		'formats': ['{0}'],
		'help': 'get station name, instrument model, and software version'
		},
	'set_hour': {
		'cmd': 'ZH',
		'format': '{0:b}',
		'args': '{hour, 0..23}',
		'formats': None,
		'help': 'set hour'
		},
	'set_minute': {
		'cmd': 'ZM',
		'format': '{0:b}',
		'args': '{hour, 0..59}',
		'formats': None,
		'help': 'set minute'
		},
	'get_time': {
		'cmd': 'ZZ',
		'format': None,
		'args': None,
		'formats': ['{0}'],
		'help': 'get logger time'
		}
	}

def thies_mkcmd (command, *args) :
	"""
	Constructs a byte-based Thies weatherstation command.
	The (optional) arguments have to be integers.
	"""
	if command not in thies_commands :
		return None
	dcmd = thies_commands[command]	# COMMAND DICTIONARY
	cmd = dcmd['cmd']
	fmt = dcmd['format']
	if fmt is None :
		nargs = 0
	else :
		nargs = len(fmt.split('{'))-1

	l = len(args)
	if nargs != l :
		logging.error ('# of arguments does not match command syntax:'+str(l)+','+str(nargs)+', '+fmt)
		return None

	b = bytearray(4+l)

	# BASIC COMMAND
	b[0] = STX
	bc = bytearray(cmd,'utf8')	# CONVERT FROM str TO BYTES
	b[1] = bc[0]
	b[2] = bc[1]
	b[-1] = ETX

	# ADD ARGUMENTS, IF ANY
	if l > 0 :
		n = 3
		for arg in args :
			b[n] = int(arg)
			n += 1
	return b

def thies_set_datetime_cmds (year,month,day,hour,minute) :
    """
    Returns an array of commands to be sent in order to set the datetime.
    """
	if year > 99 :
		year -= (year//100)*100
	cmds = []
	cmds.append (thies_mkcmd ('set_year',year+28))
	cmds.append (thies_mkcmd ('set_month',month+28))
	cmds.append (thies_mkcmd ('set_day',day+28))
	cmds.append (thies_mkcmd ('set_hour',hour))
	cmds.append (thies_mkcmd ('set_minute',minute))
    return cmds
    
def thies_get_current_weather_cmd () :
    """
    The command to be sent to get the current weather.
    """
	return thies_mkcmd ('get_time_weather')


if __name__ == '__main__' :

	print ('to get current weather, send',thies_mkcmd('get_time_weather'))
	print ('to set date to 2019-NOV-2T12:34, send',thies_set_datetime_cmds(2019,11,2,12,34))

