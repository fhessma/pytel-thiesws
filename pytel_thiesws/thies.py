# thies.py

import logging
import serial
import threading
import time

from pyobs             import PyobsModule
from pyobs.interfaces  import IWeather as IW
from pyobs_thiesws.api import get_current_weather_command

class ThiesWeatherstation (PyobsModule, IW) :
	def __init__ (self, *args, **kwargs) :
		PyobsModule.__init__ (self, thread_funcs=[self._poll_current_weather], *args, **kwargs)
		self._current_weather = None
		self._serial  = None
		self._sensors = []		# LIST OF IWeather.Sensors IN THE ORDER THEY ARE REPORTED BY WS

	@classmethod
	def default_config (cls) :
		"""
		The sensors actually present should be in a pyobs configuration file.

			sensors :
				- 'temperature'
				- 'pressure'

		etc. using the same strings as in IWeather.Sensors (case-independent) and
		in the same order as reported by the weatherstation display.
		The serial parities are 'N'|'E'|'O' and the stopbits are (1|1.5|2) as in pyserial.

		According to the Thies documentation, the data-logger uses 9600 7,E,1,True
		"""
		cfg = super(ThiesWeatherstation,cls).default_config()
		cfg['serial'] = {
			'port':'/dev/ttyUSB0',
			'baudrate':9600,
			'bytesize':7,
			'parity':'E',
			'stopbits':1,
			'xonxoff':True,
			'timeout':1.0
			}
		cfg['interval'] = 20. # SECONDS
		cfg['sensors'] = []
		return cfg

	def open (self) -> bool :
		if not PyobsModule.open (self) :
			return False

		# GET LIST OF SENSORS
		cfg = self.config
		if 'sensors' in cfg :
			sensors = cfg['sensors']	# LIST OF Enum OBJECTS
			for sensor in sensors :
				s = sensor.lower()
				for wsensor in IW.Sensors :
					ws = wsensor.value.lower()
					if s == ws :
						self._sensors.append(wsensor)
						logging.info ('adding '+ws.value+' sensor')
		if len(self._sensors) == 0 :
			logging.error ('no weather sensors configured')
			return False

		# OPEN SERIAL CONNECTION
		if self._serial is not None :
			self._serial.close()
		cfg = self.config
		try :
			port     = cfg['port']
			baud     = cfg['baudrate']
			bytesize = cfg['bytesize']
			stopbits = cfg['stopbits']
			parity   = cfg['parity']
			xonxoff  = cfg['xonxoff']
			timeout  = cfg['timeout']
			self._serial = Serial (port=port,baudrate=baud,bytesize=bytesize, \
									parity=parity,stopbits=stopbits,timeout=timeout,xonxoff=xonxoff)
			self._serial.open ()
			return self._serial.is_open

		except serial.SerialException as e :
			logging.error (str(e))
			self._serial = None
			return False

	def _get_thies_weather (self) :
		"""
		Gets a report from the weatherstation using the "mm" command with the output

			{decimal_time} {sensor1} {sensor2} ... {date} {time} CR LF
		"""
		# ASK FOR REPORT
		cmd = get_current_weather_command()
		self._serial.write (cmd)

		# GET RESPONSE
		result = str(self._serial.readline(eol=serial.to_bytes('\r\n')))
		logging.info (result)
		results = result.split()

		# CHECK RESPONSE
		if len(results) != len(self._sensors)+3 :
			logging.error ('cannot parse '+result)

		return results

	def _get_current_weather (self) :
		""" TBD : get and parse weather data from station! """

		# GET REPORT FROM WEATHERSTATION
		report = self._get_thies_weather()

		# PUT REPORT INTO DICTIONARY
		w = {}
		for i in len(self._sensors) :
			sensor = self._sensors[i]
			try :
				if sensor is IW.Sensor.RAIN :
					w[sensor.value] = bool(report[i])
				else :
					w[sensor.value] = float(report[i])
			except :
				logging.error ('cannot parse '+report[i])
				w[sensor.value] = None

		# SAVE REPORT
		self._current_weather = w

	def _poll_current_weather (self) :
		# run until cancelled
		while not self.closing.is_set () :
			# sleep interval time
			self.closing.wait (self.config['interval'])

			# GET WS INFO
			if self._serial is not None :
				try :
					self._get_current_weather ()
				except :
					logging.debug ('An error occured while reading Thies weather.')

	# IWeather

	def status (self, *args, **kwargs) -> dict :
		# s = super().status (*args, **kwargs)
		w = {k:v for k,v in self._current_weather.items()}
		return {'IWeather': w}

__all__ = ['ThiesWeatherstation']
