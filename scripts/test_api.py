# test_api.py

import serial
import sys
from pyobs_thies.api import thies_commands, thies_mkcmd, thies_parse_output

ser = None
try :
	ser = serial.Serial('/dev/ttyUSB0',timeout=0.5,baudrate=9600)
except serial.SerialException as e :
	print ('Cannot connect to device',e)
	sys.exit(1)
if not ser.is_open :
	ser.open ()

def help () :
	print ('Commands: {command} {input_as_format}')
	print ('\t{q}uit | {Q}UIT')
	print ('\tHELP | <RETURN>')
	for key,val in thies_commands.items() :
		print ('\t',key,val['format'],'\t',val['help'])

ok = True
transID = 1
print ('Thies Weatherstation test programme, Version 0.1\n==================================\n')
help()
while (ok) :
	info = None
	stuff = input ('{0:02d}> '.format(transID))
	parts = stuff.split()
	if len(parts) == 0 :
		cmd = 'HELP'
	elif len(parts) == 1 :
		cmd = stuff
	else :
		dev = parts[0].upper()
		cmd = parts[1].upper()
		if cmd in thies_commands :
			info = thies_commands[cmd]

	if cmd == '' or cmd == 'help' or cmd == 'HELP' :
		help()
	elif cmd.startswith('q') or cmd.startswith('Q') :
		ok = False

	"""
	elif cmd.startswith('<') and cmd.endswith('>') :
		ser.write(bytearray(cmd,'utf8'))
		outp = ser.readlines()
		print (outp)
		try :
			response = thies_parse_output (cmd,outp)
			print (response)
		except ValueError as e :
			print (str(e))
			print (info)
	"""

	elif cmd not in thies_commands :
		print ('Not a command!')

	else :
		if info['format'] is not None :
			fmts = info['format'].split('}')	# E.G. "{0}{1:d}" -> ["{0","{1:d"]
			inp = []
			for i in range(len(fmts)) :
				fmt = fmts[i]
				if i < len(parts)-2 :
					if fmt[-1] == 'b' :
						inp.append(int(parts[i+2]))
					else :
						inp.append(parts[i+2])
			print ('inp=',inp,tuple(inp))
			rcmd = thies_mkcmd (cmd,*tuple(inp))
		else :
			rcmd = thies_mkcmd (cmd)
		print ('\tcommand:',rcmd)

		ser.write(bytearray(rcmd,'utf8'))
		outp = ser.readlines()
		print ('\toutput:',outp)
		response = thies_parse_output (cmd,outp)
		print ('\tresponse:',response)

	transID += 1
	if transID > 99 : transID = 1

ser.close ()

