import logging

from pyobs_thies.thies import ThiesWeatherstation

print ('Opening module...')
thies = ThiesWeatherstation ()

if thies.open() :
	print (thies.status())
	thies.close()
else :
	print ('cannot open ThiesWeatherstation module')

